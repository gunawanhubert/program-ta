from PyQt5.QtCore import QDir, Qt, QUrl
from PyQt5.QtMultimedia import QMediaContent, QMediaPlayer
from PyQt5.QtMultimediaWidgets import QVideoWidget
from PyQt5.QtWidgets import (QApplication, QFileDialog, QHBoxLayout, QLabel,
        QPushButton, QSizePolicy, QSlider, QStyle, QVBoxLayout, QWidget,QGridLayout)
from PyQt5.QtWidgets import QMainWindow,QWidget, QPushButton, QAction
from PyQt5.QtGui import QIcon, QPixmap, QImage, QFont
import sys
import cv2
import face_alignment
import numpy as np
import threading
import random

import matplotlib
matplotlib.use('Qt5Agg')
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.figure import Figure


# from efficientnet.keras import EfficientNetB4

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.models import Model,Sequential
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.layers import Input, Dense, Flatten, LSTM,Embedding, TimeDistributed, Bidirectional, GRU, SimpleRNN, Dropout, GlobalAveragePooling2D, GlobalMaxPooling2D, Activation, Conv2D, MaxPooling2D, BatchNormalization, AveragePooling2D, Reshape, Permute, multiply
from tensorflow.keras.layers.experimental import preprocessing
from tensorflow.keras import layers, Input
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint, CSVLogger, TensorBoard
from keras_applications.imagenet_utils import _obtain_input_shape
import tensorflow.keras.backend as K
from tensorflow.keras.utils import get_source_inputs, get_file
import warnings


from pyVHR.signals.video import Video
from pyVHR.methods.pos import POS
from pyVHR.methods.chrom import CHROM
from pyVHR.analysis.testsuite import TestSuite, TestResult
from pyVHR.methods.base import methodFactory

# from PyQt5.QtWidgets import QApplication, QLabel


class MplCanvas(FigureCanvasQTAgg):

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        super(MplCanvas, self).__init__(fig)

class VideoPlayer(QMainWindow):
    def __init__(self):
        super().__init__()

        #CONST
        self.UNFREEZE = True
        self.FEATURE = "50BGR"

        VGG16_WEIGHTS_PATH = 'https://github.com/rcmalli/keras-vggface/releases/download/v2.0/rcmalli_vggface_tf_vgg16.h5'
        VGG16_WEIGHTS_PATH_NO_TOP = 'https://github.com/rcmalli/keras-vggface/releases/download/v2.0/rcmalli_vggface_tf_notop_vgg16.h5'


        self.RESNET50_WEIGHTS_PATH = 'https://github.com/rcmalli/keras-vggface/releases/download/v2.0/rcmalli_vggface_tf_resnet50.h5'
        self.RESNET50_WEIGHTS_PATH_NO_TOP = 'https://github.com/rcmalli/keras-vggface/releases/download/v2.0/rcmalli_vggface_tf_notop_resnet50.h5'

        self.VGGFACE_DIR = 'models/vggface'

        self.loadedFilename = ''
        self.Faces = []
        self.VHRs = []
        self.fa = face_alignment.FaceAlignment(face_alignment.LandmarksType._2D, device='cpu', face_detector='sfd')


        self.headerFont = QFont('Arial', 18)
        self.headerFont.setBold(True)

        self.predictionFont = QFont('Arial', 11)

        self.wh = 75
        self.noImage = QPixmap('noimage.jpg')
        self.noImage = self.noImage.scaled(self.wh, self.wh)

        self.setWindowTitle("PyQt5 Video Player") 

        videoLabel = QLabel("Video")
        videoLabel.setFont(self.headerFont)
        videoLabel.setContentsMargins(0,0,0,0,)
 
        self.mediaPlayer = QMediaPlayer(None, QMediaPlayer.VideoSurface)
 
        videoWidget = QVideoWidget()
 
        self.playButton = QPushButton()
        self.playButton.setEnabled(False)
        self.playButton.setIcon(self.style().standardIcon(QStyle.SP_MediaPlay))
        self.playButton.clicked.connect(self.play)
 
        self.positionSlider = QSlider(Qt.Horizontal)
        self.positionSlider.setRange(0, 0)
        self.positionSlider.sliderMoved.connect(self.setPosition)
 
        self.error = QLabel()
        self.error.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Maximum)
 
        openButton = QPushButton("Open Video")   
        openButton.setToolTip("Open Video File")
        openButton.setStatusTip("Open Video File")
        openButton.setFixedHeight(24)
        openButton.clicked.connect(self.openFile)
 
 
        # Create a widget for window contents
        wid = QWidget(self)
        self.setCentralWidget(wid)
 
        # Create layouts to place inside widget
        controlLayout = QHBoxLayout()
        controlLayout.setContentsMargins(0, 0, 0, 0)
        controlLayout.addWidget(self.playButton)
        controlLayout.addWidget(self.positionSlider)
 
        videoLayout = QVBoxLayout()
        # videoLayout.addWidget(videoLabel)
        videoLayout.addWidget(videoWidget)
        videoLayout.addLayout(controlLayout)
        videoLayout.addWidget(self.error)
        videoLayout.addWidget(openButton)
        videoLayout.setContentsMargins(0,0,0,0)
        
        rightLayout = QVBoxLayout()

        preprocessingLabel = QLabel("Preprocessing")
        preprocessingLabel.setFont(self.headerFont)
        

        framesLayout = QHBoxLayout()
        self.frame1 = QLabel("frame1")
        self.frame1.setPixmap(self.noImage)
        self.frame1.setFixedHeight(self.wh)
        self.frame1.setFixedWidth(self.wh)

        self.frame2 = QLabel("frame2")
        self.frame2.setPixmap(self.noImage)
        self.frame2.setFixedHeight(self.wh)
        self.frame2.setFixedWidth(self.wh)

        self.frame3 = QLabel("frame3")
        self.frame3.setPixmap(self.noImage)
        self.frame3.setFixedHeight(self.wh)
        self.frame3.setFixedWidth(self.wh)

        self.frame4 = QLabel("frame4")
        self.frame4.setPixmap(self.noImage)
        self.frame4.setFixedHeight(self.wh)
        self.frame4.setFixedWidth(self.wh)

        self.frame5 = QLabel("frame5")
        self.frame5.setPixmap(self.noImage)
        self.frame5.setFixedHeight(self.wh)
        self.frame5.setFixedWidth(self.wh)

        self.frame6 = QLabel("frame6")
        self.frame6.setPixmap(self.noImage)
        self.frame6.setFixedHeight(self.wh)
        self.frame6.setFixedWidth(self.wh)

        self.frame7 = QLabel("frame7")
        self.frame7.setPixmap(self.noImage)
        self.frame7.setFixedHeight(self.wh)
        self.frame7.setFixedWidth(self.wh)

        self.frame8 = QLabel("frame8")
        self.frame8.setPixmap(self.noImage)
        self.frame8.setFixedHeight(self.wh)
        self.frame8.setFixedWidth(self.wh)

        self.frame9 = QLabel("frame9")
        self.frame9.setPixmap(self.noImage)
        self.frame9.setFixedHeight(self.wh)
        self.frame9.setFixedWidth(self.wh)

        self.frame10 = QLabel("frame10")
        self.frame10.setPixmap(self.noImage)
        self.frame10.setFixedHeight(self.wh)
        self.frame10.setFixedWidth(self.wh)

        framesLayout.addWidget(self.frame1)
        framesLayout.addWidget(self.frame2)
        framesLayout.addWidget(self.frame3)
        framesLayout.addWidget(self.frame4)
        framesLayout.addWidget(self.frame5)
        framesLayout.addWidget(self.frame6)
        framesLayout.addWidget(self.frame7)
        framesLayout.addWidget(self.frame8)
        framesLayout.addWidget(self.frame9)
        framesLayout.addWidget(self.frame10)

        framesLayout2 = QHBoxLayout()
        self.frame11 = QLabel("frame1")
        self.frame11.setPixmap(self.noImage)
        self.frame11.setFixedHeight(self.wh)
        self.frame11.setFixedWidth(self.wh)

        self.frame12 = QLabel("frame2")
        self.frame12.setPixmap(self.noImage)
        self.frame12.setFixedHeight(self.wh)
        self.frame12.setFixedWidth(self.wh)

        self.frame13 = QLabel("frame3")
        self.frame13.setPixmap(self.noImage)
        self.frame13.setFixedHeight(self.wh)
        self.frame13.setFixedWidth(self.wh)

        self.frame14 = QLabel("frame4")
        self.frame14.setPixmap(self.noImage)
        self.frame14.setFixedHeight(self.wh)
        self.frame14.setFixedWidth(self.wh)

        self.frame15 = QLabel("frame5")
        self.frame15.setPixmap(self.noImage)
        self.frame15.setFixedHeight(self.wh)
        self.frame15.setFixedWidth(self.wh)

        self.frame16 = QLabel("frame6")
        self.frame16.setPixmap(self.noImage)
        self.frame16.setFixedHeight(self.wh)
        self.frame16.setFixedWidth(self.wh)

        self.frame17 = QLabel("frame7")
        self.frame17.setPixmap(self.noImage)
        self.frame17.setFixedHeight(self.wh)
        self.frame17.setFixedWidth(self.wh)

        self.frame18 = QLabel("frame8")
        self.frame18.setPixmap(self.noImage)
        self.frame18.setFixedHeight(self.wh)
        self.frame18.setFixedWidth(self.wh)

        self.frame19 = QLabel("frame9")
        self.frame19.setPixmap(self.noImage)
        self.frame19.setFixedHeight(self.wh)
        self.frame19.setFixedWidth(self.wh)

        self.frame20 = QLabel("frame10")
        self.frame20.setPixmap(self.noImage)
        self.frame20.setFixedHeight(self.wh)
        self.frame20.setFixedWidth(self.wh)

        framesLayout2.addWidget(self.frame11)
        framesLayout2.addWidget(self.frame12)
        framesLayout2.addWidget(self.frame13)
        framesLayout2.addWidget(self.frame14)
        framesLayout2.addWidget(self.frame15)
        framesLayout2.addWidget(self.frame16)
        framesLayout2.addWidget(self.frame17)
        framesLayout2.addWidget(self.frame18)
        framesLayout2.addWidget(self.frame19)
        framesLayout2.addWidget(self.frame20)

        framesLayout3 = QHBoxLayout()
        self.frame21 = QLabel("frame1")
        self.frame21.setPixmap(self.noImage)
        self.frame21.setFixedHeight(self.wh)
        self.frame21.setFixedWidth(self.wh)

        self.frame22 = QLabel("frame2")
        self.frame22.setPixmap(self.noImage)
        self.frame22.setFixedHeight(self.wh)
        self.frame22.setFixedWidth(self.wh)

        self.frame23 = QLabel("frame3")
        self.frame23.setPixmap(self.noImage)
        self.frame23.setFixedHeight(self.wh)
        self.frame23.setFixedWidth(self.wh)

        self.frame24 = QLabel("frame4")
        self.frame24.setPixmap(self.noImage)
        self.frame24.setFixedHeight(self.wh)
        self.frame24.setFixedWidth(self.wh)

        self.frame25 = QLabel("frame5")
        self.frame25.setPixmap(self.noImage)
        self.frame25.setFixedHeight(self.wh)
        self.frame25.setFixedWidth(self.wh)

        self.frame26 = QLabel("frame6")
        self.frame26.setPixmap(self.noImage)
        self.frame26.setFixedHeight(self.wh)
        self.frame26.setFixedWidth(self.wh)

        self.frame27 = QLabel("frame7")
        self.frame27.setPixmap(self.noImage)
        self.frame27.setFixedHeight(self.wh)
        self.frame27.setFixedWidth(self.wh)

        self.frame28 = QLabel("frame8")
        self.frame28.setPixmap(self.noImage)
        self.frame28.setFixedHeight(self.wh)
        self.frame28.setFixedWidth(self.wh)

        self.frame29 = QLabel("frame9")
        self.frame29.setPixmap(self.noImage)
        self.frame29.setFixedHeight(self.wh)
        self.frame29.setFixedWidth(self.wh)

        self.frame30 = QLabel("frame10")
        self.frame30.setPixmap(self.noImage)
        self.frame30.setFixedHeight(self.wh)
        self.frame30.setFixedWidth(self.wh)

        framesLayout3.addWidget(self.frame21)
        framesLayout3.addWidget(self.frame22)
        framesLayout3.addWidget(self.frame23)
        framesLayout3.addWidget(self.frame24)
        framesLayout3.addWidget(self.frame25)
        framesLayout3.addWidget(self.frame26)
        framesLayout3.addWidget(self.frame27)
        framesLayout3.addWidget(self.frame28)
        framesLayout3.addWidget(self.frame29)
        framesLayout3.addWidget(self.frame30)

        framesLayout4 = QHBoxLayout()
        self.frame31 = QLabel("frame1")
        self.frame31.setPixmap(self.noImage)
        self.frame31.setFixedHeight(self.wh)
        self.frame31.setFixedWidth(self.wh)

        self.frame32 = QLabel("frame2")
        self.frame32.setPixmap(self.noImage)
        self.frame32.setFixedHeight(self.wh)
        self.frame32.setFixedWidth(self.wh)

        self.frame33 = QLabel("frame3")
        self.frame33.setPixmap(self.noImage)
        self.frame33.setFixedHeight(self.wh)
        self.frame33.setFixedWidth(self.wh)

        self.frame34 = QLabel("frame4")
        self.frame34.setPixmap(self.noImage)
        self.frame34.setFixedHeight(self.wh)
        self.frame34.setFixedWidth(self.wh)

        self.frame35 = QLabel("frame5")
        self.frame35.setPixmap(self.noImage)
        self.frame35.setFixedHeight(self.wh)
        self.frame35.setFixedWidth(self.wh)

        self.frame36 = QLabel("frame6")
        self.frame36.setPixmap(self.noImage)
        self.frame36.setFixedHeight(self.wh)
        self.frame36.setFixedWidth(self.wh)

        self.frame37 = QLabel("frame7")
        self.frame37.setPixmap(self.noImage)
        self.frame37.setFixedHeight(self.wh)
        self.frame37.setFixedWidth(self.wh)

        self.frame38 = QLabel("frame8")
        self.frame38.setPixmap(self.noImage)
        self.frame38.setFixedHeight(self.wh)
        self.frame38.setFixedWidth(self.wh)

        self.frame39 = QLabel("frame9")
        self.frame39.setPixmap(self.noImage)
        self.frame39.setFixedHeight(self.wh)
        self.frame39.setFixedWidth(self.wh)

        self.frame40 = QLabel("frame10")
        self.frame40.setPixmap(self.noImage)
        self.frame40.setFixedHeight(self.wh)
        self.frame40.setFixedWidth(self.wh)

        framesLayout4.addWidget(self.frame31)
        framesLayout4.addWidget(self.frame32)
        framesLayout4.addWidget(self.frame33)
        framesLayout4.addWidget(self.frame34)
        framesLayout4.addWidget(self.frame35)
        framesLayout4.addWidget(self.frame36)
        framesLayout4.addWidget(self.frame37)
        framesLayout4.addWidget(self.frame38)
        framesLayout4.addWidget(self.frame39)
        framesLayout4.addWidget(self.frame40)

        framesLayout5 = QHBoxLayout()
        self.frame41 = QLabel("frame1")
        self.frame41.setPixmap(self.noImage)
        self.frame41.setFixedHeight(self.wh)
        self.frame41.setFixedWidth(self.wh)

        self.frame42 = QLabel("frame2")
        self.frame42.setPixmap(self.noImage)
        self.frame42.setFixedHeight(self.wh)
        self.frame42.setFixedWidth(self.wh)

        self.frame43 = QLabel("frame3")
        self.frame43.setPixmap(self.noImage)
        self.frame43.setFixedHeight(self.wh)
        self.frame43.setFixedWidth(self.wh)

        self.frame44 = QLabel("frame4")
        self.frame44.setPixmap(self.noImage)
        self.frame44.setFixedHeight(self.wh)
        self.frame44.setFixedWidth(self.wh)

        self.frame45 = QLabel("frame5")
        self.frame45.setPixmap(self.noImage)
        self.frame45.setFixedHeight(self.wh)
        self.frame45.setFixedWidth(self.wh)

        self.frame46 = QLabel("frame6")
        self.frame46.setPixmap(self.noImage)
        self.frame46.setFixedHeight(self.wh)
        self.frame46.setFixedWidth(self.wh)

        self.frame47 = QLabel("frame7")
        self.frame47.setPixmap(self.noImage)
        self.frame47.setFixedHeight(self.wh)
        self.frame47.setFixedWidth(self.wh)

        self.frame48 = QLabel("frame8")
        self.frame48.setPixmap(self.noImage)
        self.frame48.setFixedHeight(self.wh)
        self.frame48.setFixedWidth(self.wh)

        self.frame49 = QLabel("frame9")
        self.frame49.setPixmap(self.noImage)
        self.frame49.setFixedHeight(self.wh)
        self.frame49.setFixedWidth(self.wh)

        self.frame50 = QLabel("frame10")
        self.frame50.setPixmap(self.noImage)
        self.frame50.setFixedHeight(self.wh)
        self.frame50.setFixedWidth(self.wh)

        framesLayout5.addWidget(self.frame41)
        framesLayout5.addWidget(self.frame42)
        framesLayout5.addWidget(self.frame43)
        framesLayout5.addWidget(self.frame44)
        framesLayout5.addWidget(self.frame45)
        framesLayout5.addWidget(self.frame46)
        framesLayout5.addWidget(self.frame47)
        framesLayout5.addWidget(self.frame48)
        framesLayout5.addWidget(self.frame49)
        framesLayout5.addWidget(self.frame50)

        rPPGLayout = QHBoxLayout()
        self.canvas = MplCanvas()
        self.canvas.axes.plot(self.VHRs)
        rPPGLayout.addWidget(self.canvas)

        self.extractButton = QPushButton("Extract Frame")
        self.extractButton.clicked.connect(self.doExtract)

        self.extractVHRButton = QPushButton("Extract VHR")
        self.extractVHRButton.clicked.connect(self.doExtractVHR)


        predictionLabel = QLabel("Prediction")
        predictionLabel.setFont(self.headerFont)


        lblWh = 50
        oneFrameLayout = QHBoxLayout()
        oneFrameLabel = QLabel("1 Frame: ")
        oneFrameLabel.setFixedWidth(lblWh)
        self.oneFrameLoadModel = QPushButton("Load Model")
        self.oneFrameLoadModel.clicked.connect(self.doLoadModel1Frame)
        self.oneFramePredict = QPushButton("Predict")
        self.oneFramePredict.clicked.connect(self.doPredict1Frame)
        # self.oneFramePredictionLabel = QLabel()
        oneFrameLayout.addWidget(oneFrameLabel)
        oneFrameLayout.addWidget(self.oneFrameLoadModel)
        oneFrameLayout.addWidget(self.oneFramePredict)
        # oneFrameLayout.addWidget(self.oneFramePredictionLabel)

        fiveFrameLayout = QHBoxLayout()
        fiveFrameLabel = QLabel("10 Frames: ")
        fiveFrameLabel.setFixedWidth(lblWh)
        self.fiveFrameLoadModel = QPushButton("Load Model")
        self.fiveFrameLoadModel.clicked.connect(self.doLoadModel10Frame)
        self.fiveFramePredict = QPushButton("Predict")
        self.fiveFramePredict.clicked.connect(self.doPredict10Frame)
        # self.fiveFramePredictionLabel = QLabel()
        fiveFrameLayout.addWidget(fiveFrameLabel)
        fiveFrameLayout.addWidget(self.fiveFrameLoadModel)
        fiveFrameLayout.addWidget(self.fiveFramePredict)
        # fiveFrameLayout.addWidget(self.fiveFramePredictionLabel)

        VHRLayout = QHBoxLayout()
        VHRLabel = QLabel("VHR: ")
        VHRLabel.setFixedWidth(lblWh)
        self.VHRLoadModel = QPushButton("Load Model")
        self.VHRLoadModel.clicked.connect(self.doLoadModelVHR)
        self.VHRPredict = QPushButton("Predict")
        self.VHRPredict.clicked.connect(self.doPredictVHR)
        VHRLayout.addWidget(VHRLabel)
        VHRLayout.addWidget(self.VHRLoadModel)
        VHRLayout.addWidget(self.VHRPredict)

        CombinedLayout = QHBoxLayout()
        CombinedLabel = QLabel("Combined: ")
        CombinedLabel.setFixedWidth(lblWh)
        self.CombinedLoadModel = QPushButton("Load Model")
        self.CombinedLoadModel.clicked.connect(self.doLoadModelCombined)
        self.CombinedPredict = QPushButton("Predict")
        self.CombinedPredict.clicked.connect(self.doPredictCombined)
        CombinedLayout.addWidget(CombinedLabel)
        CombinedLayout.addWidget(self.CombinedLoadModel)
        CombinedLayout.addWidget(self.CombinedPredict)

        predictionLeftLayout = QVBoxLayout()
        predictionLeftLayout.addLayout(oneFrameLayout)
        predictionLeftLayout.addLayout(fiveFrameLayout)
        predictionLeftLayout.addLayout(VHRLayout)
        predictionLeftLayout.addLayout(CombinedLayout)
        predictionDividerLayout = QHBoxLayout()
        predictionDividerLayout.addLayout(predictionLeftLayout)
        self.predictedLabel = QLabel()
        self.predictedLabel.setFont(self.predictionFont)
        predictionDividerLayout.addWidget(self.predictedLabel)

        rightLayout.addWidget(preprocessingLabel)
        rightLayout.addLayout(framesLayout)
        rightLayout.addLayout(framesLayout2)
        # rightLayout.addLayout(framesLayout3)
        # rightLayout.addLayout(framesLayout4)
        # rightLayout.addLayout(framesLayout5)
        rightLayout.addLayout(rPPGLayout)
        rightLayout.addWidget(self.extractButton)
        rightLayout.addWidget(self.extractVHRButton)
        rightLayout.addWidget(predictionLabel)
        # rightLayout.addLayout(oneFrameLayout)
        # rightLayout.addLayout(fiveFrameLayout)
        rightLayout.addLayout(predictionDividerLayout)

        layout = QHBoxLayout()
        layout.addLayout(videoLayout)
        layout.addLayout(rightLayout)

        # Set widget to contain window contents
        wid.setLayout(layout)
 
        self.mediaPlayer.setVideoOutput(videoWidget)
        self.mediaPlayer.stateChanged.connect(self.mediaStateChanged)
        self.mediaPlayer.positionChanged.connect(self.positionChanged)
        self.mediaPlayer.durationChanged.connect(self.durationChanged)
        self.mediaPlayer.error.connect(self.handleError)
 
    def openFile(self):
        fileName, _ = QFileDialog.getOpenFileName(self, "Open Movie",
                QDir.homePath())
 
        if fileName != '':
            self.mediaPlayer.setMedia(
                    QMediaContent(QUrl.fromLocalFile(fileName)))
            self.playButton.setEnabled(True)
            self.loadedFilename = fileName
            for index, frame in enumerate([self.frame1, self.frame2, self.frame3, self.frame4, self.frame5,
                                            self.frame6, self.frame7, self.frame8, self.frame9, self.frame10,
                                            self.frame11, self.frame12, self.frame13, self.frame14, self.frame15,
                                            self.frame16, self.frame17, self.frame18, self.frame19, self.frame20,
                                            self.frame21, self.frame22, self.frame23, self.frame24, self.frame25,
                                            self.frame26, self.frame27, self.frame28, self.frame29, self.frame30,
                                            self.frame31, self.frame32, self.frame33, self.frame34, self.frame35,
                                            self.frame36, self.frame37, self.frame38, self.frame39, self.frame40,
                                            self.frame41, self.frame42, self.frame43, self.frame44, self.frame45,
                                            self.frame46, self.frame47, self.frame48, self.frame49, self.frame50]):
                frame.setPixmap(self.noImage)
                frame.setFixedHeight(self.wh)
                frame.setFixedWidth(self.wh)
            self.predictedLabel.setText("")
            self.canvas.axes.cla()
            self.canvas.draw()
 
    def exitCall(self):
        sys.exit(app.exec_())
 
    def play(self):
        if self.mediaPlayer.state() == QMediaPlayer.PlayingState:
            self.mediaPlayer.pause()
        else:
            self.mediaPlayer.play()
 
    def mediaStateChanged(self, state):
        if self.mediaPlayer.state() == QMediaPlayer.PlayingState:
            self.playButton.setIcon(
                    self.style().standardIcon(QStyle.SP_MediaPause))
        else:
            self.playButton.setIcon(
                    self.style().standardIcon(QStyle.SP_MediaPlay))
 
    def positionChanged(self, position):
        self.positionSlider.setValue(position)
 
    def durationChanged(self, duration):
        self.positionSlider.setRange(0, duration)
 
    def setPosition(self, position):
        self.mediaPlayer.setPosition(position)
 
    def handleError(self):
        self.playButton.setEnabled(False)
        self.error.setText("Error: " + self.mediaPlayer.errorString())

    def zeropad(self, vhr, length):
        # Padding for files with less than 16000 samples
        zero_padding = tf.zeros([length] - tf.shape(vhr), dtype=tf.float32)
        # Concatenate audio with padding so that all audio clips will be of the 
        # same length
        vhr = tf.cast(vhr, tf.float32)
        equal_length = tf.concat([vhr, zero_padding], 0)

        return equal_length.numpy()

    def get_spectrogram(self, vhr):
        # Padding for files with less than 16000 samples
        zero_padding = tf.zeros([22] - tf.shape(vhr), dtype=tf.float32)
        # Concatenate audio with padding so that all audio clips will be of the 
        # same length
        vhr = tf.cast(vhr, tf.float32)
        equal_length = tf.concat([vhr, zero_padding], 0)
        spectrogram = tf.signal.stft(
            equal_length, frame_length=3, frame_step=2)

        spectrogram = tf.abs(spectrogram)
        return spectrogram

    def plot_spectrogram(self, spectrogram, ax):
        # Convert to frequencies to log scale and transpose so that the time is
        # represented in the x-axis (columns). An epsilon is added to avoid log of zero.
        # log_spec = np.log(spectrogram.T+np.finfo(float).eps)
        # print(spectrogram.shape)
        log_spec = spectrogram.T
        # print(log_spec.shape)
        height = log_spec.shape[0]
        width = log_spec.shape[1]
        X = np.linspace(0, np.size(spectrogram), num=width, dtype=int)
        Y = range(height)
        # ax.pcolormesh(X, Y, log_spec, cmap = "gray")
        ax.pcolormesh(X, Y, log_spec)
    
    def doExtract(self):
        extract_thread = threading.Thread(target=self.extractFace, name="Extractor")
        extract_thread.start()
    
    def doExtractVHR(self):
        extract_vhr_thread = threading.Thread(target=self.extractVHR, name="VHRExtractor")
        extract_vhr_thread.start()

    def doLoadModel1Frame(self):
        load_model_1_frame_thread = threading.Thread(target=self.loadModel1Frame, name="LoadModel1Frame")
        load_model_1_frame_thread.start()

    def doLoadModel10Frame(self):
        load_model_10_frame_thread = threading.Thread(target=self.loadModel10Frame, name="LoadModel10Frame")
        load_model_10_frame_thread.start()

    def doLoadModelVHR(self):
        load_model_VHR_thread = threading.Thread(target=self.loadModelVHR, name="LoadModel10Frame")
        load_model_VHR_thread.start()

    def doLoadModelCombined(self):
        load_model_combined_thread = threading.Thread(target=self.loadModelCombined, name="LoadModel10Frame")
        load_model_combined_thread.start()

    def doPredict1Frame(self):
        predict_1_frame_thread = threading.Thread(target=self.predict1Frame, name="LoadModel1Frame")
        predict_1_frame_thread.start()

    def doPredict10Frame(self):
        predict_10_frame_thread = threading.Thread(target=self.predict10Frame, name="LoadModel10Frame")
        predict_10_frame_thread.start()

    def doPredictVHR(self):
        predict_VHR_thread = threading.Thread(target=self.predictVHR, name="LoadModel10Frame")
        predict_VHR_thread.start()

    def doPredictCombined(self):
        predict_comnined_thread = threading.Thread(target=self.predictCombined, name="LoadModel10Frame")
        predict_comnined_thread.start()

    def predict1Frame(self):
        self.oneFramePredict.setText("Predicting...")
        self.predictedLabel.setText("")
        prediction = self.model1frame.predict(self.Faces[0])
        prediction = np.argmax(prediction,axis=1)
        ttxt =  ["frame %s: %s\n" %(str(index + 1), "REAL" if i == 1 else "FAKE") for index, i in enumerate(prediction)]
        txt = "".join(ttxt)
        self.predictedLabel.setText(txt)
        self.oneFramePredict.setText("predict")
        return

    def predict10Frame(self):
        self.fiveFramePredict.setText("Predicting...")
        self.predictedLabel.setText("")
        prediction = self.model10frame.predict(self.Faces)
        prediction = np.argmax(prediction,axis=1)
        ttxt =  ["Seq %s: %s\n" %(str(index + 1), "REAL" if i == 1 else "FAKE") for index, i in enumerate(prediction)]
        txt = "".join(ttxt)
        self.predictedLabel.setText(txt)
        self.fiveFramePredict.setText("predict")

    def predictVHR(self):
        self.VHRPredict.setText("Predicting...")
        self.predictedLabel.setText("")
        imgs = np.reshape(self.VHRs, (self.VHRs.shape[0]))
        if imgs.shape[0] > 22:
            imgs = imgs[:22]
        imgs = self.zeropad(imgs,22)
        specto = self.get_spectrogram(imgs)
        specto = specto.numpy()
        input_vhr = np.reshape(specto, (1, specto.shape[0], specto.shape[1]))
        prediction = self.modelVHR.predict(input_vhr)
        prediction = np.argmax(prediction,axis=1)
        ttxt =  ["Seq %s: %s\n" %(str(index + 1), "REAL" if i == 1 else "FAKE") for index, i in enumerate(prediction)]
        txt = "".join(ttxt)
        self.predictedLabel.setText(txt)
        self.VHRPredict.setText("predict")

    def predictCombined(self):
        self.CombinedPredict.setText("Predicting...")
        self.predictedLabel.setText("")
        imgs = np.reshape(self.VHRs, (self.VHRs.shape[0]))
        if imgs.shape[0] > 22:
            imgs = imgs[:22]
        imgs = self.zeropad(imgs,22)
        specto = self.get_spectrogram(imgs)
        specto = specto.numpy()
        print(self.Faces[0].shape)
        input_vhr = np.reshape(specto, (1, specto.shape[0], specto.shape[1]))
        input_imgs = np.reshape(self.Faces[0], (1, self.Faces.shape[1],self.Faces.shape[2],self.Faces.shape[3],self.Faces.shape[4]))
        prediction = self.modelCombined.predict([input_imgs, input_vhr])
        prediction = np.argmax(prediction,axis=1)
        ttxt =  ["Seq %s: %s\n" %(str(index + 1), "REAL" if i == 1 else "FAKE") for index, i in enumerate(prediction)]
        txt = "".join(ttxt)
        self.predictedLabel.setText(txt)
        self.CombinedPredict.setText("predict")

    def loadModel1Frame(self):
        self.oneFrameLoadModel.setText("Loading...")
        # json_file = open("", 'r')
        # loaded_model_json = json_file.read()
        # json_file.close()
        # self.model1frame = tf.keras.models.model_from_json(loaded_model_json)
        self.model1frame = self.buildmodel1frame()
        # load weights into new model
        self.model1frame.load_weights("../model/model_vggface_1frame_50BGR_50persen.h5")
        print("Loaded model from disk")
        self.model1frame.compile(optimizer="adam", loss = "categorical_crossentropy", metrics = ["accuracy"])
        self.oneFrameLoadModel.setText("Model Loaded")

    def loadModel10Frame(self):
        self.fiveFrameLoadModel.setText("Loading...")
        # json_file = open("../model/model_vggface_10frames_skipping50BGR_lstm_70persen.json", 'r')
        # loaded_model_json = json_file.read()
        # json_file.close()
        # self.model10frame = tf.keras.models.model_from_json(loaded_model_json)
        self.model10frame = self.buildmodel10frame()
        # load weights into new model
        self.model10frame.load_weights("../model/model_vggface_10frames_skipping50BGR_lstm_70persen.h5")
        print("Loaded model from disk")
        self.model10frame.compile(optimizer="adam", loss = "categorical_crossentropy", metrics = ["accuracy"])
        self.fiveFrameLoadModel.setText("Model Loaded")

    def loadModelVHR(self):
        self.VHRLoadModel.setText("Loading...")
        # json_file = open("../model/model_vggface_10frames_skipping50BGR_lstm_70persen.json", 'r')
        # loaded_model_json = json_file.read()
        # json_file.close()
        # self.model10frame = tf.keras.models.model_from_json(loaded_model_json)
        self.modelVHR = self.buildmodelVHR()
        # load weights into new model
        self.modelVHR.load_weights("../model/model_vhr_100persen.h5")
        print("Loaded model from disk")
        self.modelVHR.compile(optimizer="adam", loss = "categorical_crossentropy", metrics = ["accuracy"])
        self.VHRLoadModel.setText("Model Loaded")

    def loadModelCombined(self):
        self.CombinedLoadModel.setText("Loading...")
        # json_file = open("../model/model_vggface_10frames_skipping50BGR_lstm_70persen.json", 'r')
        # loaded_model_json = json_file.read()
        # json_file.close()
        # self.model10frame = tf.keras.models.model_from_json(loaded_model_json)
        self.modelCombined = self.buildmodelCombined()
        # load weights into new model
        self.modelCombined.load_weights("../model/model_multi_70persen.h5")
        print("Loaded model from disk")
        self.modelCombined.compile(optimizer="adam", loss = "categorical_crossentropy", metrics = ["accuracy"])
        self.CombinedLoadModel.setText("Model Loaded")



    def extractFace(self):
        print(self.loadedFilename)
        self.Faces = []
        self.extractButton.setText("Loading...")
        cap=cv2.VideoCapture(self.loadedFilename)
        size = 256
        wi = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        hi = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
        fr = int(cap.get(cv2.CAP_PROP_FPS))
        fcount =  int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        

        minx = -1
        maxx = -1
        miny = -1
        maxy = -1
        counter = 0
        dt = 1
        ctrframe = 1
        imgs = []
        sample = 5
        while(cap.isOpened() and ctrframe<=fcount):
            _, img = cap.read()
            # gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            # faces = fc(gray)
            try:
                if(counter < (2 * sample)):
                    if(miny==-1 or True):
                        det = self.fa.get_landmarks_from_image(img)
                        if(len(det) > 0):
                            detection = det[0]
                            minx = int(min(detection[:,0]))-50
                            maxx = int(max(detection[:,0]))+50
                            miny = int(min(detection[:,1]))-200
                            maxy = int(max(detection[:,1]))+50
                            minx100 = int(min(detection[:,0]))-100
                            maxx100 = int(max(detection[:,0]))+100
                            miny100 = int(min(detection[:,1]))-400
                            maxy100 = int(max(detection[:,1]))+100
                            if(minx < 0): minx = 0
                            if(miny < 0): miny = 0
                            if(maxx > wi): maxx = wi
                            if(maxy > hi): maxy = hi
                            if(minx100 < 0): minx100 = 0
                            if(miny100 < 0): miny100 = 0
                            if(maxx100 > wi): maxx100 = wi
                            if(maxy100 > hi): maxy100 = hi
                    

                    img50 = img[miny:maxy, minx:maxx,:]
                    # img50blur = cv2.GaussianBlur(img50, (5,5))
                    # img50 = img50blur
                    # img100 = img[miny100:maxy100, minx100:maxx100,:]

                    # source_img = cv2.cvtColor(img50, cv2.COLOR_BGR2RGB)
                    source_img = cv2.resize(img50, (size, size))
                    imgs.append(source_img)

                    counter+=1
            except:
                pass
            if(ctrframe % fr == 0):
                dt+=1
                counter = 0
                print(dt)
            if(dt == 6):
                for i in range(sample):
                    self.Faces.append([])
                framecounter = 0
                indexcounter = 0
                for index, img in enumerate(imgs):
                    if(framecounter % sample):
                        framecounter = 0
                        indexcounter += 1
                    self.Faces[indexcounter % sample].append(img)
                    framecounter += 1
                self.Faces = np.asarray(self.Faces)
                # self.Faces = np.asarray(imgs)
                print(self.Faces.shape)
                # self.Faces = np.reshape(self.Faces, (1,5,size,size,3))
                break
            ctrframe += 1
        cap.release()

        for index, frame in enumerate([self.frame1, self.frame2, self.frame3, self.frame4, self.frame5,self.frame6, self.frame7, self.frame8, self.frame9, self.frame10]):
            h, w, _ = self.Faces[0][index].shape
            bytePerLine = w * 3
            disimg = cv2.cvtColor(self.Faces[0][index], cv2.COLOR_BGR2RGB)
            qImage = QImage(disimg.data, w, h, bytePerLine , QImage.Format_RGB888)
            qPixmap = QPixmap(qImage)
            qPixmap = qPixmap.scaled(self.wh, self.wh)
            frame.setPixmap(qPixmap)
            frame.setFixedHeight(self.wh)
            frame.setFixedWidth(self.wh)

        for index, frame in enumerate([self.frame11, self.frame12, self.frame13, self.frame14, self.frame15,self.frame16, self.frame17, self.frame18, self.frame19, self.frame20]):
            h, w, _ = self.Faces[1][index].shape
            bytePerLine = w * 3
            disimg = cv2.cvtColor(self.Faces[1][index], cv2.COLOR_BGR2RGB)
            qImage = QImage(disimg.data, w, h, bytePerLine , QImage.Format_RGB888)
            qPixmap = QPixmap(qImage)
            qPixmap = qPixmap.scaled(self.wh, self.wh)
            frame.setPixmap(qPixmap)
            frame.setFixedHeight(self.wh)
            frame.setFixedWidth(self.wh)

        for index, frame in enumerate([self.frame21, self.frame22, self.frame23, self.frame24, self.frame25,self.frame26, self.frame27, self.frame28, self.frame29, self.frame30]):
            h, w, _ = self.Faces[2][index].shape
            bytePerLine = w * 3
            disimg = cv2.cvtColor(self.Faces[2][index], cv2.COLOR_BGR2RGB)
            qImage = QImage(disimg.data, w, h, bytePerLine , QImage.Format_RGB888)
            qPixmap = QPixmap(qImage)
            qPixmap = qPixmap.scaled(self.wh, self.wh)
            frame.setPixmap(qPixmap)
            frame.setFixedHeight(self.wh)
            frame.setFixedWidth(self.wh)

        for index, frame in enumerate([self.frame31, self.frame32, self.frame33, self.frame34, self.frame35,self.frame36, self.frame37, self.frame38, self.frame39, self.frame40]):
            h, w, _ = self.Faces[3][index].shape
            bytePerLine = w * 3
            disimg = cv2.cvtColor(self.Faces[3][index], cv2.COLOR_BGR2RGB)
            qImage = QImage(disimg.data, w, h, bytePerLine , QImage.Format_RGB888)
            qPixmap = QPixmap(qImage)
            qPixmap = qPixmap.scaled(self.wh, self.wh)
            frame.setPixmap(qPixmap)
            frame.setFixedHeight(self.wh)
            frame.setFixedWidth(self.wh)

        for index, frame in enumerate([self.frame41, self.frame42, self.frame43, self.frame44, self.frame45,self.frame46, self.frame47, self.frame48, self.frame49, self.frame50]):
            h, w, _ = self.Faces[4][index].shape
            bytePerLine = w * 3
            disimg = cv2.cvtColor(self.Faces[4][index], cv2.COLOR_BGR2RGB)
            qImage = QImage(disimg.data, w, h, bytePerLine , QImage.Format_RGB888)
            qPixmap = QPixmap(qImage)
            qPixmap = qPixmap.scaled(self.wh, self.wh)
            frame.setPixmap(qPixmap)
            frame.setFixedHeight(self.wh)
            frame.setFixedWidth(self.wh)
        
        self.extractButton.setText("Extract Frame")

    def __merge(self,dict1, dict2):
        for key in dict2:
            if key not in dict1:
                dict1[key]= dict2[key]

    def extractVHR(self):

        defaultdict = {} 
        defaultdict["winSize"] = 1  
        defaultdict["timeStep"] = 1  
        defaultdict["methods"] = ["POS"]  
        defaultdict["zeroMeanSTDnorm"] = 0
        defaultdict["detrending"] = 0  
        defaultdict["detLambda"] = 10 
        defaultdict["BPfilter"] = 1  
        defaultdict["minHz"] = 0.75  
        defaultdict["maxHz"] = 4.0  
        videodict = {}
        videodict["startTime"] = 0  
        # videodict["endTime"] = -5  
        videodict["ROImask"] = "skin_fix"
        videodict["skinFix"] = "[40, 60]"
        videodict["evm"] = 0
        videodict["stat"] = "mean"
        m="POS"

        print(self.loadedFilename)
        self.VHRs = []
        self.extractVHRButton.setText("Loading...")
        self.canvas.axes.cla()  # Clear the canvas.
        video = Video(self.loadedFilename)
        methodsdict = {}
        methodsdict [m] = {}
        methodsdict[m]['video'] = video
        methodsdict[m]['verb'] = 1

        # -- extract faces
        video.getCroppedFaces(detector='mtcnn', extractor='skvideo')
        etime = float(5)
        self.__merge(methodsdict[m], videodict)
        method = methodFactory(m, **methodsdict[m])
        bpmES, timesES = method.runOffline(**methodsdict[m])
        print(bpmES, timesES)

        # names = self.loadedFilename.split(".mp4")
        # name = names[0].replace("_", "")
        # np.save(open(os.path.join("./outputvhr", (name+ "_"+dir+".npy")), "wb"), bpmES)
        self.VHRs = bpmES[0]
        self.canvas.axes.plot(self.VHRs)
        self.canvas.draw()
        self.extractVHRButton.setText("Extract VHR")
    
    def resnet_identity_block(self,input_tensor, kernel_size, filters, stage, block,
                          bias=False):
        filters1, filters2, filters3 = filters
        if K.image_data_format() == 'channels_last':
            bn_axis = 3
        else:
            bn_axis = 1
        conv1_reduce_name = 'conv' + str(stage) + "_" + str(block) + "_1x1_reduce"
        conv1_increase_name = 'conv' + str(stage) + "_" + str(
            block) + "_1x1_increase"
        conv3_name = 'conv' + str(stage) + "_" + str(block) + "_3x3"

        x = Conv2D(filters1, (1, 1), use_bias=bias, name=conv1_reduce_name)(
            input_tensor)
        x = BatchNormalization(axis=bn_axis, name=conv1_reduce_name + "/bn")(x)
        x = Activation('relu')(x)

        x = Conv2D(filters2, kernel_size, use_bias=bias,
                padding='same', name=conv3_name)(x)
        x = BatchNormalization(axis=bn_axis, name=conv3_name + "/bn")(x)
        x = Activation('relu')(x)

        x = Conv2D(filters3, (1, 1), use_bias=bias, name=conv1_increase_name)(x)
        x = BatchNormalization(axis=bn_axis, name=conv1_increase_name + "/bn")(x)

        x = layers.add([x, input_tensor])
        x = Activation('relu')(x)
        return x
    
    def resnet_conv_block(self,input_tensor, kernel_size, filters, stage, block,
                      strides=(2, 2), bias=False):
        filters1, filters2, filters3 = filters
        if K.image_data_format() == 'channels_last':
            bn_axis = 3
        else:
            bn_axis = 1
        conv1_reduce_name = 'conv' + str(stage) + "_" + str(block) + "_1x1_reduce"
        conv1_increase_name = 'conv' + str(stage) + "_" + str(
            block) + "_1x1_increase"
        conv1_proj_name = 'conv' + str(stage) + "_" + str(block) + "_1x1_proj"
        conv3_name = 'conv' + str(stage) + "_" + str(block) + "_3x3"

        x = Conv2D(filters1, (1, 1), strides=strides, use_bias=bias,
                name=conv1_reduce_name)(input_tensor)
        x = BatchNormalization(axis=bn_axis, name=conv1_reduce_name + "/bn")(x)
        x = Activation('relu')(x)

        x = Conv2D(filters2, kernel_size, padding='same', use_bias=bias,
                name=conv3_name)(x)
        x = BatchNormalization(axis=bn_axis, name=conv3_name + "/bn")(x)
        x = Activation('relu')(x)

        x = Conv2D(filters3, (1, 1), name=conv1_increase_name, use_bias=bias)(x)
        x = BatchNormalization(axis=bn_axis, name=conv1_increase_name + "/bn")(x)

        shortcut = Conv2D(filters3, (1, 1), strides=strides, use_bias=bias,
                        name=conv1_proj_name)(input_tensor)
        shortcut = BatchNormalization(axis=bn_axis, name=conv1_proj_name + "/bn")(
            shortcut)

        x = layers.add([x, shortcut])
        x = Activation('relu')(x)
        return x
    
    def RESNET50(self, include_top=True, weights='vggface',
             input_tensor=None, input_shape=None,
             pooling=None,
             classes=8631):
        input_shape = _obtain_input_shape(input_shape,
                                        default_size=256,
                                        min_size=32,
                                        data_format=K.image_data_format(),
                                        require_flatten=include_top,
                                        weights=weights)

        if input_tensor is None:
            img_input = Input(shape=input_shape)
        else:
            if not K.is_keras_tensor(input_tensor):
                img_input = Input(tensor=input_tensor, shape=input_shape)
            else:
                img_input = input_tensor
        if K.image_data_format() == 'channels_last':
            bn_axis = 3
        else:
            bn_axis = 1

        x = Conv2D(
            64, (7, 7), use_bias=False, strides=(2, 2), padding='same',
            name='conv1/7x7_s2')(img_input)
        x = BatchNormalization(axis=bn_axis, name='conv1/7x7_s2/bn')(x)
        x = Activation('relu')(x)
        x = MaxPooling2D((3, 3), strides=(2, 2))(x)

        x = self.resnet_conv_block(x, 3, [64, 64, 256], stage=2, block=1, strides=(1, 1))
        x = self.resnet_identity_block(x, 3, [64, 64, 256], stage=2, block=2)
        x = self.resnet_identity_block(x, 3, [64, 64, 256], stage=2, block=3)

        x = self.resnet_conv_block(x, 3, [128, 128, 512], stage=3, block=1)
        x = self.resnet_identity_block(x, 3, [128, 128, 512], stage=3, block=2)
        x = self.resnet_identity_block(x, 3, [128, 128, 512], stage=3, block=3)
        x = self.resnet_identity_block(x, 3, [128, 128, 512], stage=3, block=4)

        x = self.resnet_conv_block(x, 3, [256, 256, 1024], stage=4, block=1)
        x = self.resnet_identity_block(x, 3, [256, 256, 1024], stage=4, block=2)
        x = self.resnet_identity_block(x, 3, [256, 256, 1024], stage=4, block=3)
        x = self.resnet_identity_block(x, 3, [256, 256, 1024], stage=4, block=4)
        x = self.resnet_identity_block(x, 3, [256, 256, 1024], stage=4, block=5)
        x = self.resnet_identity_block(x, 3, [256, 256, 1024], stage=4, block=6)

        x = self.resnet_conv_block(x, 3, [512, 512, 2048], stage=5, block=1)
        x = self.resnet_identity_block(x, 3, [512, 512, 2048], stage=5, block=2)
        x = self.resnet_identity_block(x, 3, [512, 512, 2048], stage=5, block=3)

        x = AveragePooling2D((7, 7), name='avg_pool')(x)

        if include_top:
            x = Flatten()(x)
            x = Dense(classes, activation='softmax', name='classifier')(x)
        else:
            if pooling == 'avg':
                x = GlobalAveragePooling2D()(x)
            elif pooling == 'max':
                x = GlobalMaxPooling2D()(x)

        # Ensure that the model takes into account
        # any potential predecessors of `input_tensor`.
        if input_tensor is not None:
            inputs = get_source_inputs(input_tensor)
        else:
            inputs = img_input
        # Create model.
        model = Model(inputs, x, name='vggface_resnet50')

        # load weights
        if weights == 'vggface':
            if include_top:
                weights_path = get_file('rcmalli_vggface_tf_resnet50.h5',
                                        self.RESNET50_WEIGHTS_PATH,
                                        cache_subdir=self.VGGFACE_DIR)
            else:
                weights_path = get_file('rcmalli_vggface_tf_notop_resnet50.h5',
                                        self.RESNET50_WEIGHTS_PATH_NO_TOP,
                                        cache_subdir=self.VGGFACE_DIR)
            model.load_weights(weights_path)
            if K.backend() == 'theano':
                layer_utils.convert_all_kernels_in_model(model)
                if include_top:
                    maxpool = model.get_layer(name='avg_pool')
                    shape = maxpool.output_shape[1:]
                    dense = model.get_layer(name='classifier')
                    layer_utils.convert_dense_weights_data_format(dense, shape,
                                                                'channels_first')

            if K.image_data_format() == 'channels_first' and K.backend() == 'tensorflow':
                warnings.warn('You are using the TensorFlow backend, yet you '
                            'are using the Theano '
                            'image data format convention '
                            '(`image_data_format="channels_first"`). '
                            'For best performance, set '
                            '`image_data_format="channels_last"` in '
                            'your Keras config '
                            'at ~/.keras/keras.json.')
        elif weights is not None:
            model.load_weights(weights)

        return model

    def build_vgg(self):
        vgg = self.RESNET50(include_top=False, weights='vggface', input_shape=(256,256,3), pooling="avg")
        #vgg = VGGFace(model='resnet50',include_top=False, input_shape=(256, 256, 3))
        # vgg = tf.keras.applications.efficientnet.EfficientNetB4(include_top = False, weights = 'imagenet', input_shape = (256,256,3))
        vgg.trainable = False
        x = vgg.output
        x = Flatten()(x)
        model = Model(vgg.input, x)
        if self.UNFREEZE:
            for layer in model.layers[-5:]:
                if not isinstance(layer, layers.BatchNormalization):
                    layer.trainable = True
        # model.summary()
        return model

    def build_effnet(self):
        vgg = self.RESNET50(include_top=False, weights='vggface', input_shape=(256,256,3), pooling="avg")
        #vgg = VGGFace(model='resnet50',include_top=False, input_shape=(256, 256, 3))
        # vgg = tf.keras.applications.efficientnet.EfficientNetB4(include_top = False, weights = 'imagenet', input_shape = (256,256,3))
        # vgg = EfficientNetB4(include_top = False, weights = 'imagenet', input_shape = (256,256,3))
        vgg.trainable = False
        x = vgg.output
        x = Flatten()(x)
        model = Model(vgg.input, x)
        if self.UNFREEZE:
            for layer in model.layers[-5:]:
                if not isinstance(layer, layers.BatchNormalization):
                    layer.trainable = True
        # model.summary()
        return model
    
    def buildmodel10frame(self):
        input_tensor = Input(shape=(10,256,256,3))
        vggface = self.build_vgg()

        x = TimeDistributed(vggface)(input_tensor)
        # x = Flatten()(
        x = LSTM(units=64, return_sequences=False, dropout=0.5)(x)
        x = Dropout(0.3)(x)
        x = Dense(16, activation="relu")(x)
        x = Dropout(0.3)(x)
        # x = layers.BatchNormalization()(x)

        output = Dense(2, activation="softmax", name="pred")(x)

        model = Model(input_tensor, output, name='myVggface')
        # unfreeze_model(model)
        model.compile(Adam(lr=1e-4), loss = "categorical_crossentropy", metrics =["accuracy"])


        model.summary()
        return model

    def buildmodelVHR(self):
        input_tensor = Input((10,3,1))
        x= Conv2D(32, (3,3), activation="relu", padding="same")(input_tensor)
        x= Conv2D(64, (3,3), activation="relu")(x)
        x= Conv2D(32, (3,3), activation="relu", padding="same")(x)
        x = Flatten()(x)
        x = Dropout(0.3)(x)
        x = Dense(16, activation="relu")(x)
        x = Dropout(0.3)(x)
        # x = layers.BatchNormalization()(x)

        output = Dense(2, activation="softmax", name="pred")(x)

        model = Model(input_tensor, output, name='myEfNet')


        # model.summary()
        return model

    def buildmodelCombined(self):
        input_tensor_img = Input(shape=(10,256,256,3))
        # augment_layer = img_augmentation(input_tesnor)

        vggface = self.build_vgg()

        x_img = TimeDistributed(vggface)(input_tensor_img)
        # x = Flatten()(
        x_img = LSTM(units=64, return_sequences=False, dropout=0.5)(x_img)


        input_tensor_vhr = Input((10,3,1))
        x_vhr= Conv2D(32, (3,3), activation="relu", padding="same")(input_tensor_vhr)
        x_vhr= Conv2D(64, (3,3), activation="relu")(x_vhr)
        x_vhr= Conv2D(32, (3,3), activation="relu", padding="same")(x_vhr)
        x_vhr = Flatten()(x_vhr)

        x = tf.keras.layers.concatenate([x_img,x_vhr])
        x = Dropout(0.3)(x)
        x = Dense(16, activation="relu")(x)
        x = Dropout(0.3)(x)

        output = Dense(2, activation="softmax", name="pred")(x)

        model = Model([input_tensor_img,input_tensor_vhr], output, name='myEfNet')
        # model.compile(Adam(lr=1e-4), loss = "categorical_crossentropy", metrics =["accuracy"])


        model.summary()
        return model
    
    def buildmodel1frame(self):
        input_tensor = Input(shape=(256,256,3))
        # augment_layer = img_augmentation(input_tesnor)

        effnet = self.build_effnet()
        x = Dense(100, activation="relu")(effnet.output)
        x = Dense(16, activation="relu")(x)
        x = Dropout(0.3)(x)
        # x = layers.BatchNormalization()(x)

        output = Dense(2, activation="softmax", name="pred")(x)

        model = Model(effnet.input, output, name='myEfNet')
        # unfreeze_model(model)
        model.compile(Adam(lr=1e-4), loss = "categorical_crossentropy", metrics =["accuracy"])


        model.summary()
        return model
 
 
app = QApplication(sys.argv)
videoplayer = VideoPlayer()
videoplayer.resize(640, 480)
videoplayer.show()
sys.exit(app.exec_())